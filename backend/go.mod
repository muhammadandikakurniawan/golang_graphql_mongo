module gitlab.com/muhammadandikakurniawan/golang_graphql_mongo

go 1.15

require (
	github.com/99designs/gqlgen v0.13.0
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/vektah/gqlparser/v2 v2.1.0
	go.mongodb.org/mongo-driver v1.4.5
)
