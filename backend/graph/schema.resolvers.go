package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/muhammadandikakurniawan/golang_graphql_mongo/graph/generated"
	"gitlab.com/muhammadandikakurniawan/golang_graphql_mongo/graph/model"
	"gitlab.com/muhammadandikakurniawan/golang_graphql_mongo/repositories"
)

var userRepository = repositories.NewUserRepository()

func (r *mutationResolver) CreateUser(ctx context.Context, input *model.NewUser) (*model.User, error) {
	return userRepository.Create(input), nil
}

func (r *queryResolver) User(ctx context.Context, id string) (*model.User, error) {
	return userRepository.FindByID(id), nil
}

func (r *queryResolver) Users(ctx context.Context) ([]*model.User, error) {
	return userRepository.FindAll(), nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
