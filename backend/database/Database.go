package database

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func NewDB(dbName string) *DB {
	return Connect(dbName)
}

type DB struct {
	Client   *mongo.Client
	Url      string
	Database *mongo.Database
}

func Connect(dbName string) *DB {
	res := DB{
		Url:      "mongodb://localhost:27017",
		Client:   nil,
		Database: nil,
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(res.Url))
	if err == nil {
		res.Client = client
		res.Database = res.Client.Database(dbName)
	} else {
		log.Fatal(err)
	}
	return &res
}

func (this *DB) Disconnect() error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	return this.Client.Disconnect(ctx)
}

func (this *DB) InsertOne(collectionName string, document interface{}) (*mongo.InsertOneResult, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	res, err := this.Database.Collection(collectionName).InsertOne(ctx, document)
	return res, err
}
