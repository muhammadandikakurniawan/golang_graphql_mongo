import logo from './logo.svg';
import ReactDOM from "react-dom";
import React from 'react'
import './App.css';
import "semantic-ui-css/semantic.min.css";
import { Card, Icon, Image } from 'semantic-ui-react'
import { ApolloClient, InMemoryCache } from '@apollo/client';
import { gql } from '@apollo/client';

let CardExampleCard = (name, email) => (
  <Card>
    <Image src='https://react.semantic-ui.com/images/avatar/large/matthew.png' wrapped ui={false} />
    <Card.Content>
      <Card.Header>Matthew</Card.Header>
      <Card.Meta>
        <span className='date'>{name}</span>
      </Card.Meta>
      <Card.Description>
        {email}
      </Card.Description>
    </Card.Content>
  </Card>
)

class App extends React.Component{

  constructor(props){
    super(props)
    this.state = {
    }

  }

  render(){

    const client = new ApolloClient({
      uri: 'http://localhost:8080',
      cache: new InMemoryCache()
    });

    client
    .query({
      query: gql`
        query GetAllUser{
          users{
            name
            email
            _id
          }
        }
      `
    })

    return (
      <div className="App">
        {CardExampleCard("dika","dika@gmail.com")}
      </div>
    );
  }
}


export default App;
