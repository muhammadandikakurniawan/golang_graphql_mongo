package repositories

import (
	"context"
	"log"
	"time"

	"gitlab.com/muhammadandikakurniawan/golang_graphql_mongo/database"
	"gitlab.com/muhammadandikakurniawan/golang_graphql_mongo/graph/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func NewUserRepository() *UserRepository {
	db := database.NewDB("latihan")
	return &UserRepository{
		DB:             *db,
		CollectionName: "GoUser",
	}
}

type UserRepository struct {
	DB             database.DB
	CollectionName string
}

func (this *UserRepository) Create(data *model.NewUser) *model.User {
	insertRes, insertErr := this.DB.InsertOne(this.CollectionName, data)
	if insertErr != nil {
		panic(insertErr)
	}
	res := model.User{
		ID:       insertRes.InsertedID.(primitive.ObjectID).Hex(),
		Email:    data.Email,
		Name:     data.Name,
		Password: data.Password,
	}
	return &res
}

func (this *UserRepository) FindByID(id string) *model.User {
	res := model.User{}
	objectId, idErr := primitive.ObjectIDFromHex(id)
	if idErr != nil {
		// log.Fatal(idErr)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)

	defer cancel()
	filter := bson.D{{"_id", objectId}}

	err := this.DB.Database.Collection(this.CollectionName).FindOne(ctx, filter).Decode(&res)
	if err != nil {
		// log.Fatal(err)
	}
	return &res
}

func (this *UserRepository) FindAll() []*model.User {
	var results []*model.User
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)

	defer cancel()

	cur, _ := this.DB.Database.Collection(this.CollectionName).Find(ctx, bson.D{})

	for cur.Next(ctx) {
		var data *model.User

		curErr := cur.Decode(&data)

		if curErr != nil {
			log.Fatal(curErr)
		}

		results = append(results, data)

	}

	return results
}
